#!/bin/bash

setenforce 0
sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config
dnf install docker docker-compose vim -y
systemctl start docker
systemctl enable docker

cat > radio.yml << 'EOF'
version: "2.1"
services:
  airsonic:
    image: lscr.io/linuxserver/airsonic
    container_name: airsonic
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=America/Lima
###      - CONTEXT_PATH=/airsonic
###- JAVA_OPTS=<options> #optional
    volumes:
      - /home/airsonic/config:/config
      - /home/airsonic/music:/music
      - /home/airsonic/playlists:/playlists
      - /home/airsonic/podcasts:/podcasts
###      - /home/airsonic/media:/media #optional
    ports:
      - 4040:4040
###    devices:
###      - /dev/snd:/dev/snd #optional
    restart: unless-stopped
EOF

docker-compose -f radio.yml up -d

echo "ir a la web de la radio"
ip=`hostname -I | cut -f1 -d' '`
echo "$ip:4040"
